#include <libguile.h>
#include <stdio.h>
#include <memory.h>

#include "iface_type.h"

scm_t_bits flash_iface_tag;

static SCM flash_iface_mark(SCM flash_iface_smob)
{
//	struct flash_iface *iface = (struct flash_iface*)SCM_SMOB_DATA(flash_iface_snob);
	struct flash_iface *iface = EXTRACT_IFACE(flash_iface_smob);
	
	/*
	 * stub
	 */

	return SCM_BOOL_F;
}

static size_t flash_iface_free(SCM flash_iface_smob)
{
	struct flash_iface *iface = EXTRACT_IFACE(flash_iface_smob);

	scm_gc_free(iface, sizeof(struct flash_iface), "flash-iface");

	return 0;
}

static int flash_iface_print(SCM flash_iface_smob, SCM port, scm_print_state *pstate)
{
	struct flash_iface *iface = EXTRACT_IFACE(flash_iface_smob);

	return 1;
}

static SCM close_flash_iface(SCM flash_iface_smob)
{
	struct flash_iface *iface;

	scm_assert_smob_type(flash_iface_tag, flash_iface_smob);

	iface = EXTRACT_IFACE(flash_iface_smob);
	if (iface->iface_close != NULL)
		iface->iface_close(iface->handle);
	
	iface->handle = NULL;
	return SCM_BOOL_T;
}

static scm_t_uint8 *u8vector_to_array(SCM vec, size_t *len)
{
	const scm_t_uint8 *raw_vec;
	scm_t_uint8 *raw_copy;
	size_t i;
	ssize_t inc;
	scm_t_array_handle vec_handle;

	raw_vec = scm_u8vector_elements(vec, &vec_handle, len, &inc);
	raw_copy = scm_malloc(sizeof(*raw_vec)*(*len));

	for (i = 0; i < *len; ++i, raw_vec += inc)
	{
		raw_copy[i] = *raw_vec;
	}
	scm_array_handle_release(&vec_handle);
	return raw_copy;
}

static SCM array_to_u8vector(scm_t_uint8 *arr, size_t len) // copies arr
{
	scm_t_uint8 *new_block;
	size_t block_size = sizeof(*new_block)*len;

	new_block = scm_malloc(block_size);
	memcpy(new_block, arr, block_size);

	return scm_take_u8vector(new_block, len);
}

static SCM throw_io_error(const char *message)
{
	SCM sstr_error;
	sstr_error = scm_from_locale_string(message ? message : "Unknown error");
	return scm_throw(scm_from_locale_symbol("io-error"), scm_cons(sstr_error, SCM_EOL));
}

static SCM write_flash_iface(SCM flash_iface_smob, SCM vdata)
{
	struct flash_iface *iface;
	size_t data_len;
	int ret;
	const char *str_error;
	scm_t_uint8 *raw_data;

	scm_assert_smob_type(flash_iface_tag, flash_iface_smob);
	SCM_ASSERT(scm_is_true(scm_u8vector_p(vdata)), vdata, SCM_ARG2, "write-flash-iface");

	iface = EXTRACT_IFACE(flash_iface_smob);
	
	raw_data = u8vector_to_array(vdata, &data_len);
	str_error = NULL;
	ret = iface->iface_write(iface->handle, (uint8_t*)raw_data, (int)data_len, &str_error);
	free(raw_data);

	if (ret < 0)
		return throw_io_error(str_error);
	else
		return scm_from_int(ret);
}

static SCM read_flash_iface(SCM flash_iface_smob)
{
#define BUFSIZE 8192 // assume that it is enough, will rewrite later
	struct flash_iface *iface;
	int ret;
	const char *str_error;
	SCM vec;
	scm_t_uint8 *buf;

	scm_assert_smob_type(flash_iface_tag, flash_iface_smob);
	
	iface = EXTRACT_IFACE(flash_iface_smob);

	buf = scm_malloc(sizeof(*buf)*BUFSIZE);
	ret = iface->iface_read(iface->handle, buf, (int)BUFSIZE, &str_error);

	if (ret < 0) {
		free(buf);
		return throw_io_error(str_error);
	}
	vec = array_to_u8vector(buf, ret); // XXX: assuming that (size == len) is not is not good idea, hm
	free(buf);
	return vec;
}

void init_flash_iface_type(void)
{
	flash_iface_tag = scm_make_smob_type("flash-iface", sizeof(struct flash_iface));
	scm_set_smob_mark(flash_iface_tag, flash_iface_mark);
	scm_set_smob_free(flash_iface_tag, flash_iface_free);
#if 0
	scm_set_smob_print(flash_iface_tag, flash_iface_print);
#endif
	scm_c_define_gsubr("close-flash-iface", 1, 0, 0, &close_flash_iface);
	scm_c_define_gsubr("write-flash-iface", 2, 0, 0, &write_flash_iface);
	scm_c_define_gsubr("read-flash-iface", 1, 0, 0, &read_flash_iface);
}
