#include <stdio.h>
#include <libguile.h>
#include <usb.h>

#include "iface_type.h"

// TODO: rewrite this with phone *detection*
#define IFACE_VENDOR 0x22b8
#define IFACE_PRODUCT 0x4903

#define TIMEOUT 1*1000

void dump(char *buf, size_t s)
{
	int i;
	if (s == 0) return;
	for (i = 0; i < s; ++i) {
		printf("%02hhx ", (unsigned char)buf[i]);
		if (i % 16 == 15) printf("\n");
	}
	printf("\n");
}

static int init_usb()
{
	usb_init();
	usb_find_busses();
	usb_find_devices();
	return 0;
}

static int is_usb_flash_iface(struct usb_device *device)
{
	struct usb_device_descriptor *desc = &device->descriptor;
	if (desc->idVendor != IFACE_VENDOR || desc->idProduct != IFACE_PRODUCT) return 0;
	if (desc->bNumConfigurations != 1) return 0;
	return 1;
}

static struct usb_device *find_usb_flash_iface()
{
	struct usb_bus *bus;
	for (bus = usb_get_busses(); bus != NULL; bus = bus->next) {
		struct usb_device *device;
		for (device = bus->devices; device != NULL; device = device->next) {
			if (is_usb_flash_iface(device)) return device;
		}
	}
	return NULL;
}

static int close_usb_flash_iface(void *handle)
{
	if (handle) {
		fprintf(stderr, "iface destroyed\n");
		usb_close((struct usb_dev_handle*)handle);
	}
	return 0;
}

static int write_usb_flash_iface(void *handle, uint8_t *buf, int len, const char **error_msg)
{
	int ret;
	printf("WRITE: \n");
	dump(buf, len);
	ret = usb_bulk_write((struct usb_dev_handle*)handle, 0x1, (char*)buf, len, TIMEOUT);
	if (ret < 0 && error_msg) {
		*error_msg = usb_strerror();
	}
	return ret;
}

static int read_usb_flash_iface(void *handle, uint8_t *buf, int len, const char **error_msg)
{
	int ret;
	ret = usb_bulk_read((struct usb_dev_handle*)handle, 0x82, (char*)buf, len, TIMEOUT);
	printf("READ: %d\n", ret);
	if (ret < 0 && error_msg) {
		*error_msg = usb_strerror();
	} else {
		dump(buf, ret);
	}
	return ret;
}

SCM make_libusb_iface()
{
	SCM smob;
	struct flash_iface *iface;
	struct usb_device *dev;
	struct usb_dev_handle *handle;
	
	if (init_usb() != 0) {
		fprintf(stderr, "can't init usb\n");
		return SCM_BOOL_F;
	}
	dev = find_usb_flash_iface();
	if (dev == NULL) {
		fprintf(stderr, "can't find dev\n");
		return SCM_BOOL_F;
	}
	
	iface = (struct flash_iface*)scm_gc_malloc(sizeof(struct flash_iface), "flash-iface");

	handle = usb_open(dev);
	if (handle == NULL) {
		scm_gc_free(iface, sizeof(struct flash_iface), "flash-iface");
		return SCM_BOOL_F;
	}
	
	if (usb_claim_interface(handle, 0) < 0) {
		scm_gc_free(iface, sizeof(struct flash_iface), "flash-iface");
		usb_close(handle);
		return SCM_BOOL_F;
	}

	iface->handle = (void*)handle;
	iface->iface_close = &close_usb_flash_iface;
	iface->iface_write = &write_usb_flash_iface;
	iface->iface_read = &read_usb_flash_iface;

	SCM_NEWSMOB(smob, flash_iface_tag, iface);

	fprintf(stderr, "iface created\n");
	return smob;
}
