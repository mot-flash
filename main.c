#include <readline/readline.h>
#include <libguile.h>

#include "iface_type.h"

#define ASSERT_U_VECTOR(v, p, func) SCM_ASSERT(scm_is_uniform_vector(v), (v), (p), (func))
#define ASSERT_SIZE_T(v, p, func) SCM_ASSERT(scm_is_unsigned_integer(v, 0, SCM_I_SIZE_MAX), (v), (p), (func))

SCM make_libusb_iface();


SCM uniform_vector_move_x(SCM src, SCM s_start, SCM s_end, SCM dst, SCM d_start)
{
	size_t ss_start, ss_end, sd_start;
	scm_t_array_handle s_handle, d_handle;
	size_t copy_len;
	size_t s_esize;
	size_t s_len, d_len;
	ssize_t s_inc, d_inc;
	const void *raw_src;
	void *raw_dst;

	ASSERT_U_VECTOR (src    , SCM_ARG1, "uniform-vector-move!");
	ASSERT_SIZE_T   (s_start, SCM_ARG2, "uniform-vector-move!");
	ASSERT_SIZE_T   (s_end  , SCM_ARG3, "uniform-vector-move!");
	ASSERT_U_VECTOR (dst    , SCM_ARG4, "uniform-vector-move!");
	ASSERT_SIZE_T   (d_start, SCM_ARG5, "uniform-vector-move!");

	ss_start = scm_to_size_t(s_start);
	ss_end = scm_to_size_t(s_end);
	sd_start = scm_to_size_t(d_start);

	raw_src = scm_uniform_vector_elements(src, &s_handle, &s_len, &s_inc);
	if ((ss_end < ss_start) || (ss_end - ss_start > s_len)) {
		scm_array_handle_release(&s_handle);
		scm_out_of_range("uniform-vector-move!", s_end);
	}
	copy_len = (size_t)(ss_end - ss_start);

	raw_dst = scm_uniform_vector_writable_elements(dst, &d_handle, &d_len, &d_inc);
	if (copy_len + sd_start > d_len) {
		scm_array_handle_release(&s_handle);
		scm_array_handle_release(&d_handle);
		scm_out_of_range("uniform-vector-move!", s_end);
	}

	s_esize = scm_array_handle_uniform_element_size(&s_handle);
	if (s_esize != scm_array_handle_uniform_element_size(&d_handle)) {
		scm_array_handle_release(&s_handle);
		scm_array_handle_release(&d_handle);
		scm_wrong_type_arg_msg("uniform-vector-move!", SCM_ARG4, dst, "source and dest with a same type");
	}

	if (s_inc == d_inc) {
		if (d_inc == 1) {
			raw_src+=ss_start*s_inc*s_esize;
			raw_dst+=sd_start*d_inc*s_esize;
			memcpy(raw_dst, raw_src, copy_len * s_esize);
			goto out;
		} else if (s_inc == -1 && s_esize == 1) {
			raw_dst -= sd_start;
			raw_src -= ss_start;
			memcpy(raw_dst - copy_len + 1, raw_src - copy_len + 1, copy_len);
			goto out;
		}
	}
	s_inc*=s_esize;
	d_inc*=s_esize;
	raw_src+=ss_start*s_inc;
	raw_dst+=sd_start*d_inc;
	while (copy_len--) {
		memcpy(raw_dst, raw_src, s_esize);
		raw_src+=s_inc;
		raw_dst+=d_inc;
	}

out:	scm_array_handle_release(&s_handle);
	scm_array_handle_release(&d_handle);
	return SCM_UNSPECIFIED;
}

void main_repl(void *closure, int argc, char *argv[])
{
	init_flash_iface_type();
	scm_c_define_gsubr("make-libusb-iface", 0, 0, 0, &make_libusb_iface);
	scm_c_define_gsubr("uniform-vector-move!", 5, 0, 0, &uniform_vector_move_x);
	scm_c_primitive_load("common.scm");
	scm_shell(argc, argv);
	return;
}

int main(int argc, char *argv[])
{
	scm_boot_guile(argc, argv, main_repl, 0);
	return 0;
}
