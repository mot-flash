#ifndef IFACE_TYPE_H
#define IFACE_TYPE_H

#define EXTRACT_IFACE(smob) ((struct flash_iface*)SCM_SMOB_DATA(smob))

extern scm_t_bits flash_iface_tag;

struct flash_iface {
	void *handle;

	int (*iface_close)(void*);
	int (*iface_write)(void*, uint8_t*, int, const char**);
	int (*iface_read)(void*, uint8_t*, int, const char**);
};

void init_flash_iface_type(void);

#endif // IFACE_TYPE_H
